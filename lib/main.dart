import 'package:flutter/material.dart';
import 'login.dart';
import 'home.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'dart:async';
import './actions/list_action.dart';
import './actions/login_action.dart';
import './model/app_state.dart';
import './model/list_state.dart';
import './model/login_state.dart';
import './redux/reducers.dart';
import './viewmodel/_viewmodel.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Mitr',
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyAppFul()//LoginPage(),
    );
  }
}

class MyAppFul extends StatefulWidget{

  @override
  _MyAppFul createState() => _MyAppFul();
}

class _MyAppFul extends State<MyAppFul>{

  String _login = '';
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkExist();
  }

  checkExist() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isLoading = true;
      _login = prefs.getString('email');
    });
    
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return isLoading == true ? (_login == 'admin@gmail.com' ? HomePage() : LoginPage()) : Container(width:0.0, height:0.00);

  }

}
