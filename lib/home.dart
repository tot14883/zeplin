import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import './actions/list_action.dart';
import './actions/login_action.dart';
import './model/app_state.dart';
import './model/list_state.dart';
import './model/login_state.dart';
import './redux/reducers.dart';
import './viewmodel/_viewmodel.dart';
import 'package:circular_check_box/circular_check_box.dart';

import 'login.dart';

final Store store =
    Store<AppState>(appStateReducer, initialState: AppState.initialState());

class HomePage extends StatefulWidget {
  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  TextEditingController taskController = TextEditingController();

  DateTime _fromDate = DateTime.now();

  String get _labelText {
    String formattedDate = DateFormat('dd/MM/yyyy').format(_fromDate);
    var parts = formattedDate.split('/');
    int year = int.parse(parts[2]) + 543;
    var date = parts[0] + "/" + parts[1] + "/" + year.toString();
    return date;
  }

  Future<void> _showDatePicker(BuildContext context) async {
    final picked = await showDatePicker(
      context: context,
      initialDate: _fromDate,
      firstDate: DateTime(2015, 1),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _fromDate) {
      setState(() {
        _fromDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child: StoreConnector<AppState, ViewModel>(
            converter: (store) => ViewModel.create(store),
            builder: (context, ViewModel model) => Scaffold(
                  backgroundColor: Colors.white,
                  appBar: AppBar(
                    backgroundColor: Colors.white,
                    elevation: 0,
                    title: Text(
                      'To-do list',
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey,
                          fontWeight: FontWeight.bold),
                    ),
                    actions: [
                      Container(
                        margin: EdgeInsets.only(right: 18, top: 8),
                        width: 70.0,
                        height: 20.0,
                        decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: InkWell(
                            onTap: () {
                              showModalBottomSheet(
                                  isScrollControlled: true,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15),
                                        topRight: Radius.circular(15)),
                                  ),
                                  context: context,
                                  builder: (context) => Padding(
                                        padding: EdgeInsets.only(
                                            bottom: MediaQuery.of(context)
                                                .viewInsets
                                                .bottom),
                                        child: Container(
                                            height: 250,
                                            child: Column(children: [
                                              Container(
                                                margin: EdgeInsets.all(14),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    InkWell(
                                                        onTap: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                          taskController.text =
                                                              '';
                                                        },
                                                        child: Text('Cancel',
                                                            style: TextStyle(
                                                                fontSize: 16))),
                                                    Flexible(
                                                        flex: 1,
                                                        child: Container()),
                                                    InkWell(
                                                        onTap: () {
                                                          saveItem(
                                                              context, model);
                                                        },
                                                        child: Text('Save',
                                                            style: TextStyle(
                                                                fontSize: 16))),
                                                  ],
                                                ),
                                              ),
                                              Divider(),
                                              Container(
                                                  child: TextField(
                                                controller: taskController,
                                                autofocus: true,
                                                cursorColor: Colors.black,
                                                keyboardType:
                                                    TextInputType.multiline,
                                                maxLines: null,
                                                decoration: new InputDecoration(
                                                    border: InputBorder.none,
                                                    focusedBorder:
                                                        InputBorder.none,
                                                    enabledBorder:
                                                        InputBorder.none,
                                                    errorBorder:
                                                        InputBorder.none,
                                                    disabledBorder:
                                                        InputBorder.none,
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                            left: 15,
                                                            bottom: 11,
                                                            top: 11,
                                                            right: 15),
                                                    hintText: "Task name..."),
                                              )),
                                              Row(children: [
                                                Container(
                                                    margin: EdgeInsets.only(
                                                        left: 18)),
                                                Text('Due Date *',
                                                    style: TextStyle(
                                                        fontSize: 16)),
                                                Flexible(
                                                  child: Container(),
                                                  flex: 1,
                                                )
                                              ]),
                                              Container(
                                                  margin: EdgeInsets.all(18),
                                                  padding: EdgeInsets.all(12),
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      border: Border.all(
                                                          color: Colors.grey)),
                                                  child: InkWell(
                                                      onTap: () {
                                                        _showDatePicker(
                                                            context);
                                                      },
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(_labelText),
                                                          Flexible(
                                                              flex: 1,
                                                              child:
                                                                  Container()),
                                                          Icon(Icons.date_range,
                                                              color:
                                                                  Colors.green)
                                                        ],
                                                      )))
                                            ])),
                                      ));
                            },
                            child: Icon(Icons.add)),
                      )
                    ],
                  ),
                  body: SafeArea(
                      child: Column(children: [
                    Divider(),
                    ExpandablePanel(
                      header: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 18, top: 10),
                                child: Text('All',
                                    style: TextStyle(fontSize: 16))),
                          ]),
                      expanded: Divider(),
                      collapsed: Column(
                          children: model.items
                              .map((ListState item) => Column(children: [
                                    ListTile(
                                      leading: CircularCheckBox(
                                          value: item.check,
                                          checkColor: Colors.white,
                                          activeColor: Colors.green,
                                          inactiveColor: Colors.grey,
                                          disabledColor: Colors.grey,
                                          onChanged: (val){
                                                  item.check = !item.check;
                                               model.onUpdateItem(item.id, item);
                                              }),
                                      title: Text("${item.body}",
                                            style: TextStyle(
                                                decoration: item.check == true
                                                    ? TextDecoration.lineThrough
                                                    : null)),
                                      onTap: () {
                                        item.check = !item.check;
                                        model.onUpdateItem(item.id, item);
                                      },
                                    ),
                                    //RadioListTile
                                    /*CheckboxListTile(
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        title: Text("${item.body}",
                                            style: TextStyle(
                                                decoration: item.check == true
                                                    ? TextDecoration.lineThrough
                                                    : null)),
                                        value: item.check,
                                        activeColor: Colors.green,
                                        onChanged: (value) {
                                          item.check = !item.check;
                                          model.onUpdateItem(item.id, item);
                                        }),*/
                                    Divider(),
                                  ]))
                              .toList()),
                      tapHeaderToExpand: true,
                      hasIcon: true,
                    )
                  ])),
                  bottomNavigationBar: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            margin: EdgeInsets.only(bottom: 18),
                            child: InkWell(
                                onTap: () {
                                  btnLogout(context, model);
                                },
                                child: Text('Log out',
                                    style: TextStyle(fontSize: 16))))
                      ]),
                )));
  }

  btnLogout(BuildContext context, ViewModel model) async {
    await model.setLogout();

    if (await model.checkLogin() != 'admin@gmail.com') {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    }
  }

  saveItem(BuildContext context, ViewModel model) {
    model.onAddItem(taskController.text, false);
    taskController.clear();
  }
}
