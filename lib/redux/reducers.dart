import 'package:redux/redux.dart';

import '../model/login_state.dart';
import '../model/list_state.dart';
import '../model/app_state.dart';
import '../actions/login_action.dart';
import '../actions/list_action.dart';
import 'package:shared_preferences/shared_preferences.dart';

List<ListState> addItemReducer(List<ListState> state, action){
  return []..addAll(state)..add(ListState(id:action.id, body:action.item, check: action.check));
}

List<ListState> updateItemReducer(List<ListState> state, action){
  return List.unmodifiable(List.from(state)..replaceRange(action.id, action.id +1, [action.item]));

}

OnErrorState getErrorReducer(OnErrorState state, action){
  return OnErrorState("Email and Password is wrong !!");
}

Future<OnLoginState> getLoginReducer(Future<OnLoginState> state, action) async{
   SharedPreferences prefs = await SharedPreferences.getInstance();
   return OnLoginState(prefs.getString('email'));

}

final Reducer <List<ListState>> itemsReducer = combineReducers <List<ListState>>([
   new TypedReducer<List<ListState>, ListAction>(addItemReducer),
   new TypedReducer<List<ListState>, ListUpdateCheck>(updateItemReducer),
]);

final Reducer <OnErrorState> errorReducer = combineReducers <OnErrorState>([
   new TypedReducer<OnErrorState, OnError>(getErrorReducer),
]);

final Reducer <Future<OnLoginState>> loginReducer = combineReducers <Future<OnLoginState>>([
   new TypedReducer<Future<OnLoginState>, OnLogin>(getLoginReducer),
]);


AppState appStateReducer(AppState state, action){
  return AppState(
    items: itemsReducer(state.items, action),
    error: errorReducer(state.error, action),
    login: loginReducer(state.login, action)
  );
}