import 'package:flutter/foundation.dart';

class ListState {
  final String body;
  bool check;
  final int id;

  ListState({ 
   @required this.id, 
   @required this.body,
   @required this.check
  });
}