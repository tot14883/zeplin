import 'package:flutter/foundation.dart';
import 'package:zeplin/actions/login_action.dart';
import 'list_state.dart';
import 'login_state.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AppState {
  final List<ListState> items;
  //final LoginState auth;
  final OnErrorState error;
  final Future<OnLoginState> login;

  AppState({
    @required this.items,
   //@required this.auth,
    @required this.error,
    @required this.login
  });

  

  AppState.initialState()
  ://auth = LoginState('', ''),
  items = List.unmodifiable(<ListState>[]),
  error = OnErrorState(""),
  login = null;

}