class LoginState{
  String email;
  String password;
   
  LoginState(
    this.email,
    this.password
  );
}

class OnErrorState{
  String error;

  OnErrorState(
    this.error
  );
}

class OnLoginState{
  String  email;

  OnLoginState(this.email);
}