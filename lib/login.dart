import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'dart:ui';
import 'package:flutter/cupertino.dart';

import './actions/list_action.dart';
import './actions/login_action.dart';
import './model/app_state.dart';
import './model/list_state.dart';
import './model/login_state.dart';
import './redux/reducers.dart';
import './viewmodel/_viewmodel.dart';

import 'home.dart';

final Store store =
    Store<AppState>(appStateReducer, initialState: AppState.initialState());

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLogin = false;

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child: Scaffold(
            body: StoreConnector<AppState, ViewModel>(
                converter: (store) => ViewModel.create(store),
                builder: (context, ViewModel model) => 
                SafeArea(
                    child: SingleChildScrollView(
                        child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            alignment: Alignment.center,
                            child: Stack(children: [
                              Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text('Login',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24.0)),
                                    Container(
                                        margin: EdgeInsets.only(top: 18),
                                        width:
                                            MediaQuery.of(context).size.width -
                                                50,
                                        child: TextField(
                                          controller: emailController,
                                          decoration: new InputDecoration(
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(10.0),
                                                ),
                                              ),
                                              filled: true,
                                              hintStyle: new TextStyle(
                                                  color: Colors.grey[800]),
                                              hintText: "Email",
                                              fillColor: Colors.white70),
                                        )),
                                    Container(
                                        margin: EdgeInsets.only(top: 18),
                                        width:
                                            MediaQuery.of(context).size.width -
                                                50,
                                        child: TextField(
                                          controller: passwordController,
                                          obscureText: true,
                                          decoration: new InputDecoration(
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(10.0),
                                                ),
                                              ),
                                              filled: true,
                                              hintStyle: new TextStyle(
                                                  color: Colors.grey[800]),
                                              hintText: "Password",
                                              fillColor: Colors.white70),
                                        )),
                                    InkWell(
                                      onTap: () {
                                        _LoginIn(model, context);
                                      },
                                      child: Container(
                                          margin: EdgeInsets.only(top: 18),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              50,
                                          height: 50.0,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.green,
                                                  spreadRadius: 3),
                                            ],
                                          ),
                                          child: Text('Login',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16))),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 18),
                                    ),
                                    Text(model.error.error,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 16))
                                  ]),
                              /* Container(
                                child: BackdropFilter(
                                  filter:
                                      ImageFilter.blur(sigmaX: 5, sigmaY: 6),
                                  child: Container(
                                      decoration: BoxDecoration(
                                    color:
                                        Colors.grey.shade100.withOpacity(0.2),
                                  )),
                                ),
                              ),*/
                            ])))))));
  }

  _LoginIn(ViewModel model, BuildContext context) async {
    model.onLogin(emailController.text, passwordController.text);
    
    //print(await model.checkLogin());
    if(await model.checkLogin() == 'admin@gmail.com') {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    }
  }
}
