class LoginAction{
  String email;
  String password;

  LoginAction(this.email, this.password);
}

class OnError{
  String error;

  OnError(this.error);
}

class OnLogin{
  String email;

  OnLogin(this.email);
}