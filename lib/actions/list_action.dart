import '../model/list_state.dart';

class ListAction{
  static int _id = -1;
  String item;
  bool check;

  ListAction(this.item, this.check){
    _id++;
  }
   
  int get id => _id; 
}

class ListUpdateCheck{
  int id;
  ListState item;

  ListUpdateCheck(this.id, this.item);
}

