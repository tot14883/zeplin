import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/app_state.dart';
import '../model/list_state.dart';
import '../model/login_state.dart';

import '../actions/list_action.dart';
import '../actions/login_action.dart';

class ViewModel {
  //final LoginState auth;
  final List<ListState> items;
  final Function(String, String) onLogin;
  final Function(String, bool) onAddItem;
  final Function(int, ListState) onUpdateItem;
  final OnErrorState error;
  final Future<OnLoginState> login;
  

  Future<String> checkLogin() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('email');
  }

  setLogout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', '');
  }


  ViewModel({
      //this.auth,
      this.items,
      this.onAddItem,
      this.onUpdateItem,
      this.onLogin,
      this.error,
      this.login});

  factory ViewModel.create(Store<AppState> store) {
    _onAddItem(String body, bool check) {
      store.dispatch(ListAction(body, check));
    }

    _onLogin(String email, String password) async {
      if (email == 'admin@gmail.com' && password == '123456') {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('email', email);
      } else {
        store.dispatch(OnError("Email and Password is wrong"));
      }
    }

    _onUpdate(int id, ListState item){
      store.dispatch(ListUpdateCheck(id, item));
    }

    return ViewModel(
        items: store.state.items,
        login: store.state.login,
        onLogin: _onLogin,
        onAddItem: _onAddItem,
        onUpdateItem: _onUpdate,
        error: store.state.error);
  }
}
